var DEFAULT_TREE = {
  title: "Lorem",
  children: [
    {
      title: "Ipsum"
    },
    {
      title: "Dolor",
      children: [
        {
          title: "Orci",
          children: [
            {
              title: "Quis",
              children: [
                {
                  title: "Odio"
                }
              ]
            }
          ]
        }
      ]
    },
    {
      title: "Sit",
      children: [
        {
          title: "Amet"
        },
        {
          title: "Consectetur"
        }
      ]
    },
    {
      title: "Adipiscing",
      children: [
        {
          title: "Elit",
          children: [
            {
              title: "Vestibulum"
            },
            {
              title: "Vitae"
            }
          ]
        }
      ]
    }
  ]
};
