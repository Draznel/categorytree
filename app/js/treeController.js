"use strict";

function TreeRendererController()  {
    var self = this;

    self.treeItems = [];

    self.modalItem = undefined;
    self.newItemTitle = undefined;
    self.showModal = false;

    self.renderTree = function(tree, mode) {
      self.treeItems = [];
      if (mode === "recur") {
        self.renderTreeRecursively(tree);
      } else {
        self.renderTreeIteratively(tree);
      }
      self.treeItems[0].visible = true;
    };

    self.renderDefaultTreeWithMode = function(mode) {
      self.renderTree(DEFAULT_TREE, mode)
    }

    self.renderTreeRecursively = function(tree, parent, depth = 0) {
      var item = {
        title: tree.title,
        parent: parent,
        children: [],
        visible: true,
        depth: depth
      };
      if (parent) {
        parent.children.push(item);
      }
      self.treeItems.push(item);
      (tree.children || []).forEach(function(it) {
        self.renderTreeRecursively(it, item, depth + 1);
      });
    };

    self.renderTreeIteratively = function(tree) {
      var stack = [tree];
      var children, current;

      tree.depth = 0;

      while (stack.length > 0) {
        current = stack.pop();
        children = current.children || [];

        var newItem = {
          title: current.title,
          parent: current.parent,
          children: children,
          visible: true,
          depth: current.depth
        };

        self.treeItems.push(newItem);

        for (var i = children.length - 1; i >= 0; i--) {
          var it = children[i];
          it.depth = current.depth + 1;
          it.parent = newItem;
          stack.push(it);
        }
      }
    };

    self.toggleItem = function(item) {
      if (!item.children) {
        item.visible = true;
      } else {
        item.visible = !item.visible;
      }
    };

    self.parentsVisible = function(item) {
      return !item.parent || (item.parent.visible && self.parentsVisible(item.parent));
    };

    self.openModal = function(item) {
      self.modalItem = item;
      self.showModal = true;
    }

    self.addNewItem = function() {
      var index = self.treeItems.indexOf(self.modalItem);

      if ((self.modalItem.children || []).length > 0) {
        var modalItemChildren = self.modalItem.children || [];
        index = self.treeItems.indexOf(modalItemChildren[modalItemChildren.length - 1]);
      }

      var newItem = {
        title: self.newItemTitle,
        depth: self.modalItem.depth + 1,
        visible: true,
        children: [],
        parent: self.modalItem
      }
      self.treeItems.splice(index + 1, 0, newItem);
      self.modalItem.children.push(newItem);
      self.modalItem.visible = true;
      self.hideModal();
    }

    self.hideModal = function() {
      self.modalItem = undefined;
      self.showModal = false;
    }

    if (DEFAULT_TREE) {
      self.renderDefaultTreeWithMode('recur');
    }
}
